﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using WD.Feed.Crawler.Services;
using WD.Feed.Crawler.Models;
using Serilog;
using System.IO;

namespace WD.Feed.Crawler
{
    class Program
    {
        static void Main(string[] args)
        {
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            var serviceProvider = serviceCollection.BuildServiceProvider();

            serviceProvider.GetService<App>().Run();
        }
        private static void ConfigureServices(IServiceCollection serviceCollection)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.File("log_.txt", outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}",rollingInterval : RollingInterval.Day)
                .CreateLogger();
            serviceCollection.AddLogging(builder =>
            {
                builder.AddSerilog();
            });

            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false)
                .Build();
            serviceCollection.AddOptions();
            serviceCollection.Configure<AppSettings>(config.GetSection("app"));
            ConfigConsole(config);

            serviceCollection.AddTransient<ICrawlerService, CrawlerService>();
            serviceCollection.AddTransient<App>();
        }

        private static void ConfigConsole(IConfigurationRoot root)
        {
            System.Console.Title = root.GetSection("app:title").Value;
        }
    }
}
