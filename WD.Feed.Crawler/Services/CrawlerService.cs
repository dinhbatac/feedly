﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;
namespace WD.Feed.Crawler.Services
{
    public interface ICrawlerService
    {
        void Run();
    }
    public class CrawlerService : ICrawlerService
    {
       private readonly ILogger<CrawlerService> _logger;
        public CrawlerService(ILogger<CrawlerService> logger)
        {
            _logger = logger;
        }

        public void Run()
        {
           _logger.LogInformation("Start run");
        }
    }
}
