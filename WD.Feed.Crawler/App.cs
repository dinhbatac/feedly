﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WD.Feed.Crawler.Models;
using WD.Feed.Crawler.Services;

namespace WD.Feed.Crawler
{
    public class App
    {
        private readonly ICrawlerService _crawlerService;
        private readonly ILogger<App> _logger;
        private readonly AppSettings _config;

        public App(ICrawlerService crawlerService,
            IOptions<AppSettings> config,
            ILogger<App> logger)
        {
            _crawlerService = crawlerService;
            _logger = logger;
            _config = config.Value;
        }

        public void Run()
        {
            _logger.LogInformation($"This is a console application for {_config.ConsoleTitle}");
            _crawlerService.Run();
        }
    }
}
